<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bulmapress
 */
?>

</div><!-- #content -->

<!-- <footer id="colophon" class="site-footer hero is-transparent" role="contentinfo">
	<div class="container">
		<div class="hero-body has-text-centered">
			<div class="site-info">
				<?php //bulmapress_copyright_link(); ?>
			</div><!-- .site-info -->
<!-- </div> -->
<!-- </div>.container -->
<!-- </footer>  -->

<footer class="footer" style="padding: 0px;margin: 0px;   overflow: hidden !important;">
    <div class="content">
        <div class="columns has-background-red py-6-desktop hiddenINcontact">
            <div class="column is-12 ">

                <h2 class="is-family-softylus-reg has-text-centered has-text-white is-size-2 is-size-4-mobile mt-4">Don’t Hesitate To Contact Us</h2>
                <h6 class="is-family-softylus-light has-text-centered has-text-white is-size-4 is-size-6-mobile">We can’t wait to help you</h6>
                <div class="newsletterForm my-5">
                    <?php echo do_shortcode("[contact-form-7 id=\"83\" title=\"Contact form 1\"]"); ?>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="columns is-vcentered is-multiline mt-3 is-mobile">
                <div class="column is-1-desktop is-5-mobile is-6-tablet is-width46  logoCol" >
                    <figure class="image is-64x64 is-m-auto logo-footer" >
                        <img src="https://s1.softylus.com/wp-content/uploads/2020/12/128863891_197100915298871_2977430993444303554_n-removebg-preview.png">
                    </figure>
                </div>

                 <div class="column is-2-desktop is-5-mobile is-6-tablet" style="padding-left: 0px";>
                    <h1  class="is-family-softylus-bold pl-1 mt-3 has-text-gray is-size-6">SOFTYLUS</h1>
                </div>
                
                <div class="column is-hidden-desktop is-hidden-tablet  is-3-mobile">

                </div>
                <div class="column is-9-desktop is-6-mobile is-12-tablet" style="padding-top:0 !important">
                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'extra-menu',
                        'container_class' => 'footer-menu' ) );
                    ?>
                </div>
                <div class="column is-hidden-desktop is-3-mobile">

                </div>
            </div>
            <hr style="width:100%;background-color: #ececec;margin: 0.5rem 0; !important;">


            <div class="columns is-desktop">
                <div class="column has-text-left is-hidden-mobile is-hidden-tablet-only">

                    <h6 class=" is-family-softylus-light has-text-gray is-size-7">COPYRIGHT © 2020 SOFTYLUS. ALL RIGHTS RESERVED.</h6>
                </div>

                <div class="column has-text-right has-text-centered-mobile has-text-centered-tablet-only is-primary">
      <span class="icon mx-2" >
         <img class="has-text-gray icon is-small" src="https://s1.softylus.com/wp-content/uploads/2020/12/Asset-1.svg">
       </span>
                    <span class="icon mx-2" >
<img class="has-text-gray icon is-small" src="https://s1.softylus.com/wp-content/uploads/2020/12/Asset-2.svg">
                    </span>
                    <span class="icon mx-2" >
<img class="has-text-gray icon is-small" src="https://s1.softylus.com/wp-content/uploads/2020/12/Asset-3.svg">       </span>
                    <span class="icon mx-2" >
<img class="has-text-gray icon is-small" src="https://s1.softylus.com/wp-content/uploads/2020/12/Asset-4.svg">       </span>

                </div>
                <div class="column has-text-centered is-hidden-desktop">

                    <h6 class=" is-family-softylus-light has-text-gray is-size-7">COPYRIGHT © 2020 SOFTYLUS. ALL RIGHTS RESERVED.</h6>
                </div>
            </div>
        </div>
    </div>
</footer>

</div><!-- #page -->
<script src="https://cdn.jsdelivr.net/npm/bulma-carousel@4.0.3/dist/js/bulma-carousel.min.js"></script>
<script>



    bulmaCarousel.attach('#slider', {
        slidesToScroll: 1,
        slidesToShow: 3,
        infinite: true,
        autoplay: true,
    });
    // // vars
    // 'use strict'
    // var	testim = document.getElementById("testim"),
    //     testimDots = Array.prototype.slice.call(document.getElementById("testim-dots").children),
    //     testimContent = Array.prototype.slice.call(document.getElementById("testim-content").children),
    //     testimLeftArrow = document.getElementById("left-arrow"),
    //     testimRightArrow = document.getElementById("right-arrow"),
    //     testimSpeed = 4500,
    //     currentSlide = 0,
    //     currentActive = 0,
    //     testimTimer,
    //     touchStartPos,
    //     touchEndPos,
    //     touchPosDiff,
    //     ignoreTouch = 30;
    // ;
    //
    // window.onload = function() {
    //
    //     // Testim Script
    //     function playSlide(slide) {
    //         for (var k = 0; k < testimDots.length; k++) {
    //             testimContent[k].classList.remove("active");
    //             testimContent[k].classList.remove("inactive");
    //             testimDots[k].classList.remove("active");
    //         }
    //
    //         if (slide < 0) {
    //             slide = currentSlide = testimContent.length-1;
    //         }
    //
    //         if (slide > testimContent.length - 1) {
    //             slide = currentSlide = 0;
    //         }
    //
    //         if (currentActive != currentSlide) {
    //             testimContent[currentActive].classList.add("inactive");
    //         }
    //         testimContent[slide].classList.add("active");
    //         testimDots[slide].classList.add("active");
    //
    //         currentActive = currentSlide;
    //
    //         clearTimeout(testimTimer);
    //         testimTimer = setTimeout(function() {
    //             playSlide(currentSlide += 1);
    //         }, testimSpeed)
    //     }
    //
    //     testimLeftArrow.addEventListener("click", function() {
    //         playSlide(currentSlide -= 1);
    //     })
    //
    //     testimRightArrow.addEventListener("click", function() {
    //         playSlide(currentSlide += 1);
    //     })
    //
    //     for (var l = 0; l < testimDots.length; l++) {
    //         testimDots[l].addEventListener("click", function() {
    //             playSlide(currentSlide = testimDots.indexOf(this));
    //         })
    //     }
    //
    //     playSlide(currentSlide);
    //
    //     // keyboard shortcuts
    //     document.addEventListener("keyup", function(e) {
    //         switch (e.keyCode) {
    //             case 37:
    //                 testimLeftArrow.click();
    //                 break;
    //
    //             case 39:
    //                 testimRightArrow.click();
    //                 break;
    //
    //             case 39:
    //                 testimRightArrow.click();
    //                 break;
    //
    //             default:
    //                 break;
    //         }
    //     })
    //
    //     testim.addEventListener("touchstart", function(e) {
    //         touchStartPos = e.changedTouches[0].clientX;
    //     })
    //
    //     testim.addEventListener("touchend", function(e) {
    //         touchEndPos = e.changedTouches[0].clientX;
    //
    //         touchPosDiff = touchStartPos - touchEndPos;
    //
    //         console.log(touchPosDiff);
    //         console.log(touchStartPos);
    //         console.log(touchEndPos);
    //
    //
    //         if (touchPosDiff > 0 + ignoreTouch) {
    //             testimLeftArrow.click();
    //         } else if (touchPosDiff < 0 - ignoreTouch) {
    //             testimRightArrow.click();
    //         } else {
    //             return;
    //         }
    //
    //     })
    // }
        
    
 
// email video auto play
// document.getElementById('videoId').play();
</script>

<!--<script type="text/javascript" src="/node_modules/bulma-accordion/dist/js/bulma-accordion.min.js"></script>-->
<script type="text/javascript">
    function changeBGcolor() {
        // Get the checkbox
        var checkBox = document.getElementById("accInput");
        // Get the li
        var li = document.getElementById("accLi");

        // If the checkbox is unchecked, change color
        if (checkBox.checked){
            li.style.backgroundColor= "transparent !important";
        } else {
           li.style.backgroundColor = "rgba(203, 2, 2, 0.15) !important";
        }

    }
    //      $(function () {
//          $(".facebook").on({
//              mouseenter: function () {
//                  $(".facebookImg").attr('src', 'https://ik.imagekit.io/softylus/facebook-logo-in-circular-shape_1_white_ojrLBdCe9-.png');
//              },
//              mouseleave: function () {
//                  $(".facebookImg").attr('src', 'https://ik.imagekit.io/softylus/facebook-logo-in-circular-shape_LwG0yai_T.svg');
//              }
//          });

//      });

//  $(function () {
//      $(".ppc").on({
//          mouseenter: function () {
//              $(".ppcImg").attr('src', 'https://ik.imagekit.io/softylus/pay-per-click__2__1_white_zoWPRWK0fB.png');
//          },
//          mouseleave: function () {
//              $(".ppcImg").attr('src', 'https://ik.imagekit.io/softylus/pay-per-click__2__cOscI52MxQAH.svg');
//          }
//      });

//  });

//  $(function () {
//      $(".local").on({
//          mouseenter: function () {
//              $(".localImg").attr('src', 'https://ik.imagekit.io/softylus/local__2__1_white_e7VwKchSgS.png');
//          },
//          mouseleave: function () {
//              $(".localImg").attr('src', 'https://ik.imagekit.io/softylus/local__2__quyZREeCV.svg');
//          }
//      });

//  });

//  $(function () {
//      $(".emailM").on({
//          mouseenter: function () {
//              $(".emailMImg").attr('src', 'https://ik.imagekit.io/softylus/email-marketing__1__1_white_LZk8Zipb13.png');
//          },
//          mouseleave: function () {
//              $(".emailMImg").attr('src', 'https://ik.imagekit.io/softylus/email-marketing__1__9Jkg7cThK.svg');
//          }
//      });

//  });



//  $(function () {
//      $(".video").on({
//          mouseenter: function () {
//              $(".videoImg").attr('src', 'https://ik.imagekit.io/softylus/icon_whit_qH_W-Jk_nx.svg');
//          },
//          mouseleave: function () {
//              $(".videoImg").attr('src', 'https://ik.imagekit.io/softylus/nn_5i7p8BfnK2gn.svg');
//          }
//      });

//  });

//  $(function () {
//      $(".socilaAd").on({
//          mouseenter: function () {
//              $(".socilaAdImg").attr('src', 'https://ik.imagekit.io/softylus/socilaAd_1_white_jbPM3TPqK4FSe.png');
//          },
//          mouseleave: function () {
//              $(".socilaAdImg").attr('src', 'https://ik.imagekit.io/softylus/socilaAd_Y_-IS8O7zaZ7.svg');
//          }
//      });

//  });

//  $(function () {
//      $(".contentt").on({
//          mouseenter: function () {
//              $(".contentImg").attr('src', 'https://ik.imagekit.io/softylus/content_1white_Wtp0YhJgh9.svg');
//          },
//          mouseleave: function () {
//              $(".contentImg").attr('src', 'https://ik.imagekit.io/softylus/content_1red_J7XfxMI_Q.svg');
//          }
//      });

//  });
//  $(function () {
//      $(".seo").on({
//          mouseenter: function () {
//              $(".seoImg").attr('src', 'https://ik.imagekit.io/softylus/web_z1MfBIJukOmFj.svg');
//          },
//          mouseleave: function () {
//              $(".seoImg").attr('src', 'https://ik.imagekit.io/softylus/iconnn_RteGeaHlSQrKD.svg');
//          }
//      });

//  });
//  $(function () {
//      $(".linkedIn").on({
//          mouseenter: function () {
//              $(".linkedInImg").attr('src', 'https://ik.imagekit.io/softylus/Group_74_1_white_Ch9rFlu7vqzh4.png');
//          },
//          mouseleave: function () {
//              $(".linkedInImg").attr('src', 'https://ik.imagekit.io/softylus/Group_74_zOhfZHd8hhNQY.svg');
//          }
//      });

//  });

//  $(function () {
//      $(".convertion").on({
//          mouseenter: function () {
//              $(".convertionImg").attr('src', 'https://ik.imagekit.io/softylus/iconn_1_white_XF7nNPffi-mob.png');
//          },
//          mouseleave: function () {
//              $(".convertionImg").attr('src', 'https://ik.imagekit.io/softylus/iconn_DzdL-Y58R.svg');

//          }
//      });
//  });
 //features in mobile development pagee
 //     $(function () {
 //         $(".performance").on({
 //             mouseenter: function () {
 //                 $(".performanceImg").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/performance-white.svg');
 //             },
 //             mouseleave: function () {
 //                 $(".performanceImg").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/performance2.svg');
 //
 //             }
 //         });
 //     });
 //     $(function () {
 //         $(".secure").on({
 //             mouseenter: function () {
 //                 $(".secureImg").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/secure-white.svg');
 //             },
 //             mouseleave: function () {
 //                 $(".secureImg").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/secure2.svg');
 //
 //             }
 //         });
 //     });
 //     $(function () {
 //         $(".custom").on({
 //             mouseenter: function () {
 //                 $(".customImg").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/cusomization-white.svg');
 //             },
 //             mouseleave: function () {
 //                 $(".customImg").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/cusomization2.svg');
 //
 //             }
 //         });
 //     });
 //     $(function () {
 //         $(".creative").on({
 //             mouseenter: function () {
 //                 $(".creativeImg").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/creative-display-white.svg');
 //             },
 //             mouseleave: function () {
 //                 $(".creativeImg").attr('src', 'https://s1.softylus.com/wp-content/uploads/2020/12/creative-display2.svg');
 //
 //             }
 //         });
 //     });
</script>
<script>
// mobile menu
   const burgerIcon = document.querySelector('#burger');
    const navbarMenu = document.querySelector('#nav-links');

    burgerIcon.addEventListener('click', () => {
        navbarMenu.classList.toggle('is-active');
    });
</script>
<script>

    const nav = document.querySelector('#nav');
    const nav_ = document.querySelector('.nav_');
    const footercontent = document.querySelector('.footer');
    const site_content = document.querySelector('.site-content');
    const menuToggle = document.querySelector('.nav__toggle');
    const portfolio = document.querySelector('#portfolio');
    let isMenuOpen = false;


    // TOGGLE MENU ACTIVE STATE
    menuToggle.addEventListener('click', e => {
        e.preventDefault();
        isMenuOpen = !isMenuOpen;

        // toggle a11y attributes and active class
        menuToggle.setAttribute('aria-expanded', String(isMenuOpen));
        nav_.hidden = !isMenuOpen;
        nav.classList.toggle('nav--open');
        site_content.classList.toggle('hide');
        footercontent.classList.toggle('hide');
        portfolio.classList.toggle('hide');
    });



    // TRAP TAB INSIDE NAV WHEN OPEN
    // nav.addEventListener('keydown', e => {
    //     // abort if menu isn't open or modifier keys are pressed
    //     if (!isMenuOpen || e.ctrlKey || e.metaKey || e.altKey) {
    //         return;
    //     }
    //
    //     // listen for tab press and move focus
    //     // if we're on either end of the navigation
    //     const menuLinks = menu.querySelectorAll(a);
    //     if (e.keyCode === 9) {
    //         if (e.shiftKey) {
    //             if (document.activeElement === menuLinks[0]) {
    //                 menuToggle.focus();
    //                 e.preventDefault();
    //             }
    //         } else if (document.activeElement === menuToggle) {
    //             menuLinks[0].focus();
    //             e.preventDefault();
    //         }
    //     }
    // });
    const menu = document.querySelector(".menu");
    const items = document.querySelectorAll(".item");

    /* Activate Submenu */
    function toggleItem() {
        if (this.classList.contains("sub-menu-active")) {
            this.classList.remove("sub-menu-active");
        } else if (menu.querySelector(".sub-menu-active")) {
            menu.querySelector(".sub-menu-active").classList.remove("sub-menu-active");
            this.classList.add("sub-menu-active");
        } else {
            this.classList.add("sub-menu-active");
        }
    }

    /* Close Submenu From Anywhere */
    function closeSubmenu(e) {
        let isClickInside = menu.contains(e.target);

        if (!isClickInside && menu.querySelector(".sub-menu-active")) {
            menu.querySelector(".sub-menu-active").classList.remove("sub-menu-active");
        }
    }
    /* Event Listeners */
    for (let item of items) {
        if (item.querySelector(".sub-menu")) {
            item.addEventListener("click", toggleItem, false);
        }
        item.addEventListener("keypress", toggleItem, false);
    }
    document.addEventListener("click", closeSubmenu, false);
</script>
<?php wp_footer(); ?>

</body>
</html>
