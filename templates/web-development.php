<?php
/**
 * template Name: web-development
 * @package Bulmapress
 */

get_header(); ?>
<!-- test -->
<div id="primary" class="site-content has-background-white">
    <div id="content" role="main" style="overflow: hidden;">
        <section class="container" >
            <div class="columns p-6 is-vcentered flip">
                 <div class="column is-half flip-down has-text-centered-mobile ">
                    <span class="small-header mt-2">WEB DEVELOPMENT</span>
                    <h1 class="mb-3 line-height is-uppercase is-size-2-mobile is-size-1 is-family-softylus-black">Develop with <span class="has-text-red">us</span></h1>
                  
                    <p class="is-family-softylus-reg is-size-5 px-6-mobile">
                 No matter the number, we're here to help! We build you the website of your dream.

                    </p>
                    <button class="btn mt-5 is-family-softylus-bold has-background-red is-size-6 has-text-white is-uppercase">
                        get started
                    </button>
                </div>
                <div class="column is-half flip-up">
                    <figure class="image mb-2">
                        <img src="https://ik.imagekit.io/softylus/hexa_Rtx1yyjob.jpg">
                    </figure>
                </div>
            </div>

        </section>
        <section class="section">
            <div class="container">
                <div class="columns ">
                    <div class="column has-text-centered is-mobile is-full" >
                        <span class="small-header">How we help you?</span>
                        <h1 class="my-5 line-height is-size-4-mobile is-uppercase is-size-1 is-family-softylus-black">Our Features</h1>
                        <p style="line-height:1.4 ;" class="is-family-softylus-reg line-height-p my-5 px-6 is-size-5 is-size-6-mobile">
                            We  obsess over the delivery of tangible results such as leads & sales. Metric
                            impact your bottom line. Our approach remains unchanged across our services from SEO,
                            Social Media Marketing to Web & App Development.
                        </p>
                    </div>
                </div>
                <div class="columns mt-6 is-full  ">
                    <div class="columns myBox responsive bg-white">
                        <div class="column is-one-fifth ">
                            <figure class=" has-text-centered-mobile responsiveM is-mobile">
                                <img class="image is-96x96 is-m-auto responsiveImg" src="https://s1.softylus.com/wp-content/uploads/2020/12/responsive-red2.svg">
                            </figure>
                        </div>
                        <div class="column has-text-centered-mobile is-mobile ">
                            <h2 class="is-family-softylus-black my-3 font-24 has-text-centered-mobile is-uppercase is-mobile ">Responsive</h2>
                            <p class="pr-3">Perfect website features that give your user’s mobile experience no less than that of a desktop’s.</p>
                            <button class="button mt-5 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                        </div>
                    </div>

                    <div class="column is-1">


                    </div>
                    <div class="columns myBox friendly bg-white">
                        <div class="column  is-one-fifth ">
                            <figure class="has-text-centered-mobile friendlyM is-mobile">
                                <img class="image is-96x96 is-m-auto friendlyImg" src="https://s1.softylus.com/wp-content/uploads/2020/12/user-friendly-red2.svg">
                            </figure>
                        </div>
                        <div class="column has-text-centered-mobile is-mobile ">
                            <h2 class="is-family-softylus-black my-3 font-24 has-text-centered-mobile is-uppercase is-mobile ">user friendly</h2>
                            <p class="pr-3">Save your user’s time looking for the information they need with good usability.</p>
                            <button class="button mt-5 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                        </div>
                    </div>

                </div>
                <!-- end the section 1  -->

                <div class="columns mt-6 is-full  ">
                    <div class="columns myBox seo1 bg-white">
                        <div class="column is-one-fifth ">
                            <figure class=" has-text-centered-mobile is-mobile">
                                <img class="image is-96x96 is-m-auto seoImg1"  src="https://s1.softylus.com/wp-content/uploads/2020/12/seo-red2.svg">
                            </figure>
                        </div>
                        <div class="column has-text-centered-mobile is-mobile ">
                            <h2 class="is-family-softylus-black my-3 font-24 has-text-centered-mobile is-uppercase is-mobile ">search engine optimized</h2>
                            <p  class="pr-3">Attract the right traffic to your website with better quality.</p>
                            <button class="button mt-5 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                        </div>
                    </div>
                    <div class="column is-1">


                    </div>
                    <div class="columns myBox performance bg-white">
                        <div class="column is-one-fifth ">
                            <figure class="has-text-centered-mobile is-mobile">
                                <img  class="image is-96x96 is-m-auto performanceImg" src="https://s1.softylus.com/wp-content/uploads/2020/12/performance2.svg">
                            </figure>
                        </div>
                        <div class="column has-text-centered-mobile is-mobile ">
                            <h2 class="is-family-softylus-black my-3 has-text-centered-mobile is-mobile is-uppercase font-24">heigh performance</h2>
                            <p class="pr-3">Compelling features that best suit all types of apps to comply with the users’ expectations</p>
                            <button class="button mt-5 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    <section class="container" >
        <div class="columns py-6 is-vcentered">
            <div class="column is-half p-6">
                <figure class="image">
                    <img src="https://s1.softylus.com/wp-content/uploads/2020/12/22.png">
                </figure>
            </div>
            <div class="column is-half p-6 has-text-centered-mobile">
                <h2 class="mb-4 is-uppercase line-height is-size-1 is-size-2-mobile is-family-softylus-black">Name it and we <span class="has-text-red">have</span> it </h2>
                <p class="is-family-softylus-reg is-size-5">our team of experts and graphic designers is top-notch when it comes to research and delivery skills. You won't have to worry about meeting your audience needs, as we include you with every step of the game right from the get-go till the end of the project and even after.</p>
            </div>
        </div>

    </section>
        <section style="background-color:#F0F0F0">
        <div class="container py-6"  >
            <div class="columns px-4 is-vcentered flip">

                <div class="column is-half flip-down has-text-centered-mobile">
                    <h2 class="mb-4 is-uppercase line-height is-size-1 is-size-2-mobile is-family-softylus-black"> in a <span class="has-text-red">responsive</span> way</h2>
                    <p class="is-family-softylus-reg is-size-5">Our responsive website designs guarantee you the best results on small and big screens. Consider how people are glued to their phones and use it <br> as your chance to take up the rankings! The deal gets better as we offer you free enterprise-class security to protect your website from hacks and malware.</p>
                </div>
                <div class="column is-half p-3 flip-up">
                    <figure class="image p-3">
                        <img src="https://ik.imagekit.io/softylus/responsive-hex_KzunKCpEh.png">
                    </figure>
                </div>
            </div>

        </div>
        </section>
        <section class="container" >
            <div class="columns py-6 is-vcentered">
                <div class="column is-half px-6">
                    <figure class="image">
                        <img src="https://ik.imagekit.io/softylus/performance_image_0aONpOIlQ.png">
                    </figure>
                </div>
                <div class="column is-half p-6 has-text-centered-mobile">
                    <h2 class="mb-4 is-uppercase line-height is-size-1 is-size-2-mobile is-family-softylus-black">fast and  <span class="has-text-red">outstanding</span></h2>
                    <p class="is-family-softylus-reg is-size-5">If you're willing to take-up the rankings, you'll have to keep up with the increasing number of web requests. For this we make sure the performance is fast and outstanding, leaving no room for dissatisfied users! Leave it to us and your user experience will be one worth a try.</p>
                </div>
            </div>

        </section>
        <section class="our-services container " style="margin-bottom: 150px">
            <h2 class=" title px-3 is-2 has-text-black is-family-softylus-black  is-uppercase has-text-centered is-size-3-mobile">Our <span class="has-text-red">Services</span></h2>
            <hr class=" mb-6" style="width:15%;background-color:#CB0202;margin: auto;">
            <div class="columns is-multiline is-mobile px-5 has-text-centered-mobile">
                <div class="column is-6-tablet is-6-mobile is-3-desktop has-text-centered">
                    <figure>
                        <img class="serviceIcon image" src="https://s1.softylus.com/wp-content/uploads/2020/12/webDevelopment.svg">
                    </figure>
                    <h3 style="fon-size:15px !important;"  class="is-family-softylus-black">Web Development</h3>
                    <span class="is-family-softylus-hairline">Building websites with various themes</span>
                </div>
                <div class="column is-6-tablet is-6-mobile is-3-desktop has-text-centered">
                    <figure>
                        <img class="serviceIcon image" src="https://s1.softylus.com/wp-content/uploads/2020/12/mobileDevelopment.svg">
                    </figure>
                    <h3 style="fon-size:15px !important;" class="is-family-softylus-black">Mobile Development</h3>
                    <span class="is-family-softylus-hairline">Building highly responsive mobile apps</span>
                </div>
                <div class="column is-6-tablet is-6-mobile is-3-desktop has-text-centered">
                    <figure>
                        <img class="serviceIcon image" src="https://s1.softylus.com/wp-content/uploads/2020/12/design.svg">
                    </figure>
                    <h3  style="fon-size:15px !important;"  class="is-family-softylus-black">Design</h3>
                    <span class="is-family-softylus-hairline">Taking Care of the visual aesthetics of your needs</span>
                </div>
                <div class="column is-6-tablet is-6-mobile is-3-desktop has-text-centered">
                    <figure>
                        <img class="serviceIcon image" src="https://s1.softylus.com/wp-content/uploads/2020/12/digitalMarketing-1.svg">
                    </figure>
                    <h3 style="fon-size:15px !important;"  class="is-family-softylus-black">Digital Marketing </h3>
                    <span class="is-family-softylus-hairline">From content to campaigns, we make the plans happen</span>
                </div>

            </div>
        </section>
    </div>
</div>
<script
        src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs="
        crossorigin="anonymous"></script>
<script src="https://s1.softylus.com/wp-content/themes/softylus/templates/replaceimg.js" >
</script>
<?php get_footer(); ?>
