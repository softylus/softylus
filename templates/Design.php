<?php
/**
 * template Name: Design 
 * @package Bulmapress
 */

get_header(); ?>
<!-- test -->
<div id="primary" class="site-content has-background-white">
    <div id="content" role="main" style="overflow-y: hidden;">
        <section class="section" style="padding-top: 0.5rem !important;">
            <div class="container">
              <div class="columns columnss is-vcentered">
                <div class="column  first_div  is-half has-text-centered-mobile">
                    <span class="small-header">DESIGN</span>
                    <h1 class="mb-3 line-height is-uppercase is-size-2-mobile is-size-1 is-family-softylus-black">Let’s Sketch up Some <br><span class="has-text-red">Designs!</span></h1>
                  <p class="is-family-softylus-reg is-size-5 px-6-mobile is-size-6-mobile">
                       No matter the number, we're here to help! We build you the website of your dream.
                    </p>
                  
                      <button class="button mt-5 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                </div>
                <div class="column second_div has-text-centered-mobile is-half">
                    <figure class="">
                        <img class="design-iamge" src="https://s1.softylus.com/wp-content/uploads/2020/12/slider-iamges.png">
                    </figure>
                </div>
            </div>
        </div>
        </section>

        
          <section class="section p-1 bg-gray">
            <div class="container">
              <div class="columns">
                 <div class="column has-text-centered-mobile  m-t is-mobile is-half">
                    <figure class="">
                        <img class="" style="width:65% !important;" src="https://s1.softylus.com/wp-content/uploads/2020/12/coffee-bar-logo-design-1.png">
                    </figure>
                </div>
                <div class="column has-text-centered-mobile is-mobile is-half">
                   <h1 class="my-5 line-height is-size-2-mobile is-uppercase is-size-1 is-family-softylus-black">Logo Design</h1>
                    <p style="line-height:1.4 ;" class="is-family-softylus-reg line-height-p px-2 is-size-4 is-size-6-mobile">
                        Names for people make-up identity and logos for business stand up for who and what the brand is all about. At Softylus we understand how customers tend to be fascinated by the first look and so we communicate your brand values through stunning logos that are catchy and easily remembered by everyone.
                    </p>
                      <button class="button my-5 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                </div>
              </div>
            </div>
          </section>

           <section class="section ">
            <div class="container">
              <div class="columns  columnss">
                   <div class="column has-text-centered-mobile m-t is-mobile first_div ">
                   <h1 class="my-5 line-height is-size-2-mobile is-uppercase is-size-1 is-family-softylus-black">Web Design </h1>
                    <p style="line-height:1.4 ;" class="is-family-softylus-reg line-height-p px-2 is-size-4 is-size-6-mobile">
                        Our designs consider two things: the latest trends in the industry, and function. We design your website so it’s capable of giving your visitors the best user experience using advanced visual designs and well-put elements that suit your website’s theme and services.</p>
                      <button class="button mt-5 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                </div>
                 <div class="column is-1 is-hidden-mobile ">
                  
                </div>
                 <div class="column has-text-centered-mobile is-mobile m-t is-half second_div">
                    <figure class="">
                        <img class="" src="https://s1.softylus.com/wp-content/uploads/2020/12/Asset-4-1.png">
                    </figure>
                </div>
               
              </div>
            </div>
          </section>
          
           <section class="section p-1 section-background-img ">
            <div class="container">
              <div class="columns">    
                 <div style="" class="column  is-half">
                   
                </div>
                <div class="column margin-section column-color-white has-text-centered-mobile is-mobile is-half">
                   <h1 class="my-5 px-4 line-height  is-uppercase is-size-1 is-family-softylus-black is-size-2-mobile">Illustration or graphics </h1>
                    <p style="line-height:1.4 ;" class="is-family-softylus-reg px-4 line-height-p is-size-4 is-size-6-mobile ">
                        We understand that sometimes you might need custom-designed illustrations and graphics to reflect your website or marketing plans and campaigns better. That’s why we offer you the chance to have custom-made ones that provide you with what you can’t find from the limited options out there.
                    </p>
                      <button class="button my-5 mx-3 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                </div>
              </div>
            </div>
          </section>
          
           <section class="section my-6  poster-background-img ">
            <div class="container">
              <div class="columns is-multiline">    
                 <div style="" class="column has-text-centered-mobile is-mobile  is-full">
                   <h1 style="color: #fff;line-height: 0.9; font-family:'Softylus-black';" class=" my-6 poster_font  has-text-centered-mobile">Poster Design</h1>
                     <p style="line-height:1.4;color: rgba(256,256,256,.7);text-align:left;" class=" poster_P is-family-softylus-reg has-text-centered-mobile is-size-4-mobile mobile-padding">
                         Posters are pretty much needed for advertising and marketing purposes regardless of the type of your business or the announcement you’d like to highlight. This is why we offer you poster design as a service, to help you communicate your ideas using the proper photos, illustrations, and typography so don’t sweat it.
                     </p>
                </div>
               
              </div>
            </div>
          </section>
            <section class="section ">
            <div class="container">
              <div class="columns  columnss">
                   <div class="column has-text-centered-mobile is-mobile first_div ">
                   <h1 class="my-5 line-height px-3 is-uppercase is-size-1 is-family-softylus-black is-size-2-mobile">Email Template </h1>
                    <p style="line-height:1.4 ;" class="is-family-softylus-reg  px-3 line-height-p is-size-4 is-size-6-mobile">
                        Creating an appealing email design template is a must for email marketing campaigns and we consider that so the template is looking good on all devices while including the essential details and without forgetting any.
                    </p>
                      <button class="button mt-5 mx-3 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                </div>
                 <div class="column is-1 is-hidden-mobile is-hidden-tablet">
                  
                </div>
               
                 <div class="column has-text-centered-mobile is-mobile is-half second_div">
                   <video autoplay loop muted id="videoId" >
                        <source src="https://s1.softylus.com/wp-content/uploads/2020/12/Custom-EM-design_Animation.mp4" type="video/mp4">
                    </video>
                </div>
               
              </div>
            </div>
          </section>
           <section class="section p-1 bg-gray">
            <div class="container">
              <div class="columns ">
                 <div class="column is-half">
                    <figure class="">
                        <img class="px-6"  src="https://s1.softylus.com/wp-content/uploads/2020/12/Business-card.png">
                    </figure>
                </div>
                <div class="column has-text-centered-mobile is-mobile is-half">
                   <h1 class="my-5 line-height  is-uppercase is-size-1 is-family-softylus-black is-size-2-mobile">Business Card <br>Design</h1>
                    <p style="line-height:1.4 ;" class="is-family-softylus-reg line-height-p is-size-4 px-4 is-size-6-mobile">
                        Getting your brand recognizable through business cards is still an important step to take, from displaying your business logo to including the details everything is elegantly placed on every single card design that we create.
                    </p>
                      <button class="button my-6 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                </div>
              </div>
            </div>
          </section>
    </div>
</div>
<?php 
get_footer();
?>