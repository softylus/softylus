<?php
/**
 * template Name: Marketing 
 * @package Bulmapress
 */

get_header(); ?>
<!-- test -->
<div id="primary" class="site-content has-background-white">
    <div id="content" role="main" style="overflow-y: hidden;">
        <section class="section" style="padding-top: 0.5rem !important;">
            <div class="container">
              <div class="columns columnss is-vcentered">
                <div class="column  first_div is-vcentered has-text-centered-mobile mb-6">
                    <span class="small-header">MARKETING</span>
                    <h1 class="mb-3 line-height is-uppercase is-size-2-mobile is-size-1 is-family-softylus-black">Grow Bigger <br><span class="has-text-red">  With Us!</span></h1>
                    <p class="is-family-softylus-reg line-height-p is-size-5">
                        Watch your plans turn into real projects with a fast response team to answer your needs within one business day.
                    </p>
                    <a href="<?php echo esc_attr( esc_url( get_page_link( 172 ) ) ) ?>" style="cursor: pointer">
                        <button class="button my-5 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Let’s chat</button>
                    </a>
                </div>
                <div class="column second_div has-text-centered-mobile is-7">
                    <figure class="px-6-desktop py-6-desktop">
                        <img class="px-6-desktop" src="https://ik.imagekit.io/softylus/Group_1_sMSnAtmepnywd.svg">
                    </figure>
                </div>
            </div>
        </div>
        </section>

        
          <section class="section  py-0-mobile">
            <div class="container">
              <div class="columns">
                 <div class="column has-text-centered-mobile  m-t is-mobile is-half">
                    <figure class="">
                        <img class=""  src="https://ik.imagekit.io/softylus/Chart_1_Hvy73YgrGWb.svg">
                    </figure>
                </div>
               
                <div class="column has-text-centered-mobile is-mobile">
                   <h2 class="my-5 line-height is-size-2-mobile is-uppercase is-size-1 is-family-softylus-black">We help generate visitors to your site.</h2>
                    <p style="line-height:1.4 ;" class="is-family-softylus-reg line-height-p px-2 is-size-5 is-size-6-mobile">
                        By building up your website from scratch so it is user-friendly and running at full speed on all devices, with eye-catching designs and easy to navigate layouts that get your users the best experience once they land on your website’s page.
                    </p>
                </div>
              </div>
            </div>
          </section>

         <section class="section  ">
            <div class="container">
              <div class="columns flip">
                <div class="column has-text-centered-mobile is-mobile flip-down">
                   <h2 class="my-5 line-height is-size-2-mobile is-uppercase is-size-1 is-family-softylus-black">Growing analytics</h2>
                    <p style="line-height:1.4 ;" class="is-family-softylus-reg line-height-p px-2 is-size-5 is-size-6-mobile">
                        Watch your data grow bigger and your charts rise higher, as we optimize your website in a way that makes it easy for customers to purchase and make decisions that will help you close more deals. Business is all about data-driven choices!
                    </p>
                </div>
                <div class="column is-1 is-hidden-mobile ">
                  
                </div>
                 <div class="column has-text-centered-mobile  m-t is-mobile is-half flip-up">
                    <figure class="">
                        <img class="" src="https://ik.imagekit.io/softylus/Chart_2_bdt6ik4wu.svg">
                    </figure>
                </div>
              </div>
            </div>
          </section>
     
          <section class="section bg-gray"">
            <div class="container">
                <div class="columns ">
                    <div class="column has-text-centered is-mobile is-full" >
                   <span class="small-header">How we help you?</span>     
                    <h2 class="my-5 line-height is-size-2-mobile is-uppercase is-size-1 is-family-softylus-black">Our Services</h2>
                     <p style="line-height:1.4 ;" class="is-family-softylus-reg line-height-p my-5 px-6 is-size-5 is-size-6-mobile">
                         Your all in one option to set up your business targets, from building up websites and apps to mapping out the suitable marketing plan.
                     </p>
                    </div>
                </div>   
                   <div class="columns mt-6 is-full  ">
                  <div class="columns myBox contentt bg-white">
                            <div class="column is-one-fifth ">
                           <figure class=" is-96x96 has-text-centered-mobile emailM is-mobile">
                            <img class="p-3 contentImg" src="https://ik.imagekit.io/softylus/content_1red_J7XfxMI_Q.svg">
                          </figure>
                      </div>
                         <div class="column has-text-centered-mobile is-mobile ">
                            <h3 class="is-family-softylus-black my-3 font-24 has-text-centered-mobile is-mobile ">CONTENT MARKETING</h3>
                          <p class="pr-3">Wording content that serves your business goals
                          </p>
                          </div>
                      </div> 

                         <div class="column is-1">


                      </div>
                      <div class="columns myBox emailM bg-white">
                            <div class="column  is-one-fifth ">
                           <figure class=" is-96x96 has-text-centered-mobile emailM is-mobile">
                            <img class="p-3 emailMImg" src="https://ik.imagekit.io/softylus/email-marketing__1__9Jkg7cThK.svg">
                          </figure>
                      </div>
                         <div class="column has-text-centered-mobile is-mobile ">
                            <h3 class="is-family-softylus-black my-3 font-24 has-text-centered-mobile is-mobile ">EMAIL MARKETING CAMPAIGNS</h3>
                          <p class="pr-3">Manage and track your email campaigns with easy tools
                          </p>
                          </div>
                      </div> 

                </div>   
                 <!-- end the section 1  -->

                 <div class="columns mt-6 is-full  ">
                      <div class="columns myBox facebook bg-white">
                            <div class="column is-one-fifth ">
                           <figure class=" is-96x96 has-text-centered-mobile is-mobile">
                            <img class="p-3 facebookImg"  src="https://ik.imagekit.io/softylus/facebook-logo-in-circular-shape_LwG0yai_T.svg">
                          </figure>
                      </div>
                         <div class="column has-text-centered-mobile is-mobile ">
                            <h3 class="is-family-softylus-black my-3 font-24 has-text-centered-mobile is-mobile ">FACEBOOK ADVERTISING CAMPAIGNS</h3>
                          <p  class="pr-3">Building up campaigns to reach your target audience
                          </p>
                           </div>
                      </div>
                         <div class="column is-1">


                      </div>
                      <div class="columns myBox convertion bg-white">
                            <div class="column is-one-fifth ">
                           <figure class=" is-96x96 has-text-centered-mobile is-mobile">
                            <img  class="p-3 convertionImg" src="https://ik.imagekit.io/softylus/iconn_DzdL-Y58R.svg">
                          </figure>
                      </div>
                         <div class="column has-text-centered-mobile is-mobile ">
                            <h3 class="is-family-softylus-black my-3 has-text-centered-mobile is-mobile  font-24">CONVERSION RATE OPTIMIZATION</h3>
                          <p class="pr-3">Increase your visitors and turn them into customers
                          </p>
                          </div>
                      </div>
                  </div> 
 
         <!-- end the section 2  -->

                   <div class="columns mt-6 is-full  ">
                      <div class="columns myBox local bg-white">
                            <div class="column is-one-fifth ">
                           <figure class=" is-96x96 has-text-centered-mobile is-mobile">
                            <img class="p-3 localImg" src="https://ik.imagekit.io/softylus/local__2__quyZREeCV.svg">
                          </figure>
                      </div>
                         <div class="column has-text-centered-mobile is-mobile ">
                            <h3 class="is-family-softylus-black my-3 font-24 has-text-centered-mobile is-mobile ">LOCAL SEO</h3>
                          <p  class="">Accentuate your online presence to potential nearby customers
                          </p>
                           </div>
                      </div>
                         <div class="column is-1">
                      </div>

                      <div class="columns myBox ppc bg-white">
                            <div class="column is-one-fifth ">
                           <figure class="is-96x96 has-text-centered-mobile is-mobile">
                            <img class="p-3 ppcImg" src="https://ik.imagekit.io/softylus/pay-per-click__2__cOscI52MxQAH.svg">
                          </figure>
                        </div>
                         <div class="column has-text-centered-mobile is-mobile ">
                            <h3 class="is-family-softylus-black my-3 font-24 has-text-centered-mobile is-mobile ">PAY PER CLICK (PPC)</h3>
                          <p class="pr-3">Turn clicks into profitable numbers for your business .</p>
                          </div>
                      </div>
                  </div> 

              <!-- end the section 3  -->
                   
                   <div class="columns mt-6 is-full  ">
                      <div class="columns myBox linkedIn bg-white">
                            <div class="column is-one-fifth ">
                           <figure class=" is-96x96 has-text-centered-mobile is-mobile">
                            <img class="p-3 linkedInImg "src="https://ik.imagekit.io/softylus/Group_74_zOhfZHd8hhNQY.svg">
                          </figure>
                      </div>
                         <div class="column has-text-centered-mobile is-mobile ">
                            <h3 class="is-family-softylus-black my-3 font-24 has-text-centered-mobile is-mobile ">LINKEDIN ADVERTISING CAMPAIGNS</h3>
                          <p  class="pr-3">Increase your reach and build connections professionally</p>
                           </div>
                      </div>
                         <div class="column is-1">
                      </div>
                      
                      <div class="columns myBox socilaAd bg-white">
                            <div class="column is-one-fifth ">
                           <figure class=" is-96x96 has-text-centered-mobile is-mobile ">
                            <img class="p-3 socilaAdImg" src="https://ik.imagekit.io/softylus/socilaAd_Y_-IS8O7zaZ7.svg">
                          </figure>
                      </div>
                         <div class="column has-text-centered-mobile is-mobile ">
                            <h3 class="is-family-softylus-black my-3 font-24">SOCIAL MEDIA MARKETING</h3>
                          <p class="pr-3">Promoting your services on different social network service platforms</p>
                          </div>
                      </div>
                  </div> 

                   
                    <!-- end the section 4  -->
               

                <div class="columns mt-6 is-full  ">
                      <div class="columns myBox seo bg-white">
                            <div class="column is-one-fifth ">
                           <figure class=" is-96x96 has-text-centered-mobile is-mobile">
                            <img class="p-3 seoImg" src="https://ik.imagekit.io/softylus/iconnn_RteGeaHlSQrKD.svg">
                          </figure>
                      </div>
                         <div class="column has-text-centered-mobile is-mobile " >
                            <h3 class="is-family-softylus-black my-3 font-24">SEARCH ENGINE OPTIMIZATION (SEO)</h3>
                          <p  class="pr-3">Creating results to snatch better ranks on search engines</p>
                           </div>
                      </div>
                         <div class="column is-1">
                      </div>
                      
                      <div class="columns myBox video bg-white">
                            <div class="column is-one-fifth ">
                           <figure class=" is-96x96 has-text-centered-mobile is-mobile">
                            <img class="p-3 videoImg" src="https://ik.imagekit.io/softylus/nn_5i7p8BfnK2gn.svg">
                          </figure>
                      </div>
                         <div class="column is-mobile has-text-centered-mobile is-mobile ">
                            <h3 class="is-family-softylus-black my-3 font-24">VIDEO MARKETING</h3>
                          <p class="pr-3">Transform your marketing strategy with video ads</p>
                          </div>
                      </div>
                  </div> 

                        <!-- end the section 5  -->
                </div>  

            </div>
         </div>
          </section>

          <section class="section">
             <div class="container">
              <div class="columns">   
                  <div class="column has-text-centered is-mobile is-full" >
               
                    <h2 class="my-5 line-height is-size-2-mobile is-uppercase is-size-1 is-family-softylus-black">So What Do You Think !</h2>
                     <p style="line-height:1.4 ;" class="is-family-softylus-reg line-height-p my-5 px-6 is-size-5 is-size-6-mobile">
                         We are here to plant your business ideas where they should be seen and discovered
                     </p>
                      <a href="<?php echo esc_attr( esc_url( get_page_link( 172 ) ) ) ?>" style="cursor: pointer">
                      <button  class="button mt-5 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Contact us</button>
                      </a>

                  </div>
              </div>
            </div>
          </section>
        </body>
    </div>
</div>
 <script
  src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
  integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs="
  crossorigin="anonymous"></script>
<script src="https://s1.softylus.com/wp-content/themes/softylus/templates/replaceimg.js" >
</script>
<!--    class="عزيزي ${FirstName} <br> رمز التحقق (OTP) لإعادة تعيين كلمة المرور الخاصة بك هو ${OTP}. رمز التحقق صالح لمدة 10 دقائق"-->
<?php 
get_footer();
?>