<?php
/**
 * template Name: About Us
 * @package Bulmapress
 */

get_header(); ?>
<meta name="description" content=" Whatever goals that you have in mind, we pour our hearts and souls to get them done as they should, and because we don’t take our customers’ dreams lightly we put the efforts needed so you could have the best experience and polished designs.
"/>
<!-- test -->
<div id="primary" class="site-content has-background-white">
    <div id="content" role="main" style="overflow-y: hidden;">
        <section class="mx-4" >
            <div class="container">
              <div class="columns columnss pt-6 is-vcentered">
                <div style="" class="column is-full-mobile first_div has-text-centered-mobile  is-half">
                    <span class="small-header">ABOUT US</span>
                    <h1 class="mb-3 line-height is-uppercase is-size-2-mobile is-size-1 is-family-softylus-black">Stop! Take a Moment and Know What It’s Like to<span class="has-text-red"> Be With Us </span></h1>
                    <p class="is-family-softylus-reg is-size-5 px-6-mobile is-size-6-mobile">
               Your journey with us gets the most attention out of everything that we work on and for, we put our customers, we mean you, on top of everything so you’re never behind in any progress that is done.


                    </p>
                      <button class="button mt-5 px-6 font-weight-smaill is-red is-uppercase is-rounded "><a class="has-text-white" href="#portfolio">view portfolio</a></button>
                </div>
                <div class="column second_div has-text-centered-mobile is-half">
                    <figure class="is-hidden-touch">
                        <img style="margin-bottom: -1%;" src="https://ik.imagekit.io/softylus/Group_13_BnWHRPFbk.svg">
                    </figure>
                          <figure class="px-6 is-hidden-desktop">
                        <img style="margin-bottom: -1%;" src="https://ik.imagekit.io/softylus/about_us_hero_N9sVOUNALIo3b.png">
                    </figure>
                </div>
            </div>
        </div>
        </section>

      <section style="overflow: hidden; width: 100%;"class="img-background is-hidden-mobile ">
        <div class=" ">
         <div class="columns is-multiline ">
           <div class="column is-3">
           </div>
           <div class="column is-3 mt-6 has-text-centered-mobile is-mobile">
            <h1 class="mb-5 px-4 line-height is-uppercase is-size-2-mobile is-size-1 is-family-softylus-black">Get a Sneak Peak of Your Journey  <br class="is-hidden-mobile">with us</h1>
           </div>
             <div class="column is-3 about-box lunch mt-6 mx-5 ">
                 <div class="columns is-multiline">
                   <div class="column is-full">
                     <figure class=" is-96x96 has-text-centered-mobile is-mobile">
                            <img class="p-3 lunchImg" src="https://ik.imagekit.io/softylus/Group_3_kDvpA42NAm3LV.svg">
                        </figure>
                   </div>
                   <div class="column is-full">
                        <h2 class="is-family-softylus-black has-text-red mb-3 px-3 font-24">Discovering Your Needs</h2>
                         <p class="px-3">You might have a rough idea of what you want, but not necessarily what you need. Don’t worry, that's where our part comes to ensure that you’re walking on the right track without feeling lost.
</p>
                                <button class="button my-5 mx-3 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                   </div>
                   
                 </div>
           </div>

             <div class="column is-3 mx-2">
           </div>

            <div style="margin-top: -8% !important;  height: 450px;" class="column is-3  my-5   happy about-box">
              <div class="columns is-multiline">
                   <div class="column is-full">
                     <figure class=" is-96x96 has-text-centered-mobile is-mobile">
                            <img class="p-3 happyImg" src="https://ik.imagekit.io/softylus/Group_2_pjV5mgSyANcDa.svg">
                        </figure>
                   </div>
                   <div class="column is-full">
                        <h2 class="is-family-softylus-black  has-text-red px-3 mb-3 font-24">Writing The Plot For Your Plan</h2>
                         <p class="px-3">A plot can’t be without all the side and main characters, and your business can’t be done without attending to all the small and big details of the project. Tiny or not, for us everything is major if it’s gonna give you the story you’re looking for.
</p>
                                <button class="button mt-5 mx-3 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                   </div>
                   
                 </div>
           </div>
           <div class="column is-3 my-3 mx-2  plan about-box ">
             <div class="columns is-multiline">
                   <div class="column is-full">
                     <figure class=" is-96x96 has-text-centered-mobile is-mobile">
                            <img class="p-3 planImg" src="https://ik.imagekit.io/softylus/Group_4_f_-7nunP4X9L.svg">
                        </figure>
                   </div>
                   <div class="column is-full">
                        <h2 class="is-family-softylus-black has-text-red  px-3 my-3 font-24"> Make it Real</h2>
                         <p class="px-3">We walk the talk and transfer what once was an abstract idea into real business and put explendid efforts so your project could steal the show and see the light.</p>
                                <button class="button my-5 mx-3 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                   </div>
                   
                 </div>
           </div>
             <div class="column is-3 my-3 mx-2 ">

           </div>
             <div style="margin-top: -3% !important;" class="column is-3 my-3  creation about-box">
                   <div class="columns is-multiline">
                   <div class="column is-full">
                     <figure class=" is-96x96 has-text-centered-mobile is-mobile">
                            <img class="p-3 creationImg" src="https://ik.imagekit.io/softylus/Group_5_icBNu696h.svg">
                        </figure>
                   </div>
                   <div class="column px-3 is-full">
                        <h2 class="is-family-softylus-black has-text-red my-3 px-3 font-24">Get The Happy Ending You want</h2>
                         <p class="px-3">Our clients are the main characters of their stories and we’re the experts in making them the heroes they dream of. Failing your trust is never an option on our list, and giving you the smile you want is an ultimate goal of ours.</p>
                                <button class="button my-5 mx-3 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                   </div>
                   
                 </div>
           </div>

              <div class="column is-3 mx-2 ">
           </div>
           <div class="column is-3 my-3  mx-2  ">
           
           </div>
             <div class="column is-3 my-3 mx-2 ">

           </div>
             <div class="column is-3 my-3">
           </div>


         </div>
         
        </div>
       </section>
    
         <!-- only on mobile and tablet  -->
 <section class="section img-background  is-hidden-desktop">
            <div class="container">
                <div class="columns ">
                    <div class="column has-text-centered is-mobile is-full" >
              
                    <h1 class="my-5 line-height is-size-2-mobile is-uppercase is-size-1 is-family-softylus-black">Discovering Your Needs</h1>
                     <p style="line-height:1.4 ;" class="is-family-softylus-reg line-height-p my-5 px-3 is-size-5 is-size-6-mobile">
                      You might have a rough idea of what you want, but not necessarily what you need. Don’t worry, that's where our part comes to ensure that you’re walking on the right track without feeling lost.
                    </p>
                    </div>
                </div>   
                   <div class="columns mt-6 is-full  ">
                  <div class="columns myBox contentt bg-white">
                            <div class="column is-one-fifth ">
                           <figure class=" is-96x96 has-text-centered-mobile  is-mobile">
                            <img class="p-3 " src="https://ik.imagekit.io/softylus/Group_4_f_-7nunP4X9L.svg">
                          </figure>
                      </div>
                         <div class="column has-text-centered-mobile is-mobile ">
                            <h2 class="is-family-softylus-black my-3 font-24 has-text-centered-mobile is-mobile ">Writing The Plot For Your Plan</h2>
                          <p class="pr-3">A plot can’t be without all the side and main characters, and your business can’t be done without attending to all the small and big details of the project. Tiny or not, for us everything is major if it’s gonna give you the story you’re looking for.
</p>
                                <button class="button mt-5 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                          </div>         
                      </div> 

                         <div class="column is-1">


                      </div>
                      <div class="columns myBox  bg-white">
                            <div class="column  is-one-fifth ">
                           <figure class=" is-96x96 has-text-centered-mobile emailM is-mobile">
                            <img class="p-3 " src="https://ik.imagekit.io/softylus/Group_3_kDvpA42NAm3LV.svg">
                          </figure>
                      </div>
                         <div class="column has-text-centered-mobile is-mobile ">
                            <h2 class="is-family-softylus-black my-3 font-24 has-text-centered-mobile is-mobile ">Make it Real</h2>
                          <p class="pr-3">We walk the talk and transfer what once was an abstract idea into real business and put explendid efforts so your project could steal the show and see the light.</p>
                                <button class="button mt-5 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                          </div>         
                      </div> 

                </div>   
                 <!-- end the section 1  -->

                
         <!-- end the section 2  -->

                   <div class="columns mt-6 is-full  ">
                      <div class="columns myBox  bg-white">
                            <div class="column is-one-fifth ">
                           <figure class=" is-96x96 has-text-centered-mobile is-mobile">
                            <img class="p-3 " src="https://ik.imagekit.io/softylus/Group_2_pjV5mgSyANcDa.svg">
                          </figure>
                      </div>
                         <div class="column has-text-centered-mobile is-mobile ">
                            <h2 class="is-family-softylus-black my-3 font-24 has-text-centered-mobile is-mobile ">Get The Happy Ending You want</h2>
                          <p  class="">WOur clients are the main characters of their stories and we’re the experts in making them the heroes they dream of. Failing your trust is never an option on our list, and giving you the smile you want is an ultimate goal of ours.
                          </p>
                                <button class="button mt-5 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                           </div>
                      </div>
                         <div class="column is-1">
                      </div>

                      <div class="columns myBox  bg-white">
                            <div class="column is-one-fifth ">
                           <figure class="is-96x96 has-text-centered-mobile is-mobile">
                            <img class="p-3 " src="https://ik.imagekit.io/softylus/Group_5_icBNu696h.svg">
                          </figure>
                        </div>
                         <div class="column has-text-centered-mobile is-mobile ">
                            <h2 class="is-family-softylus-black my-3 font-24 has-text-centered-mobile is-mobile ">Make Them Happy</h2>
                          <p class="pr-3">We help you to promote your brand to billions of users on Instagram,
                               Facebook, and Twitter with the best strategies.</p>
                                <button class="button mt-5 px-6 font-weight-smaill is-red is-uppercase is-rounded ">Read More</button>
                          </div>         
                      </div>
                  </div> 

              <!-- end the section 3  -->
                   
                 

                        <!-- end the section 5  -->
                </div>  

            </div>
         </div>
          </section>


   <section class="section img-background-bm">
            <div class="container">
              <div class="columns is-multiline">
                 <div class="column has-text-centered-mobile is-mobile is-half">
                    <figure class="">
                        <img class=""  src="https://ik.imagekit.io/softylus/Cards_1__MJXSW_7PI.svg">
                    </figure>
                </div>
                <div class="column has-text-centered-mobile is-mobile is-half">
                   <h1 class="my-5 mt-6 line-height is-size-2-mobile is-uppercase is-size-1 is-family-softylus-black">We Develop Every Single Pixel For You!</h1>
                    <p style="line-height:1.4 ; color:#666 !important" class=" line-height-p px-2 is-size-4 is-size-6-mobile">
                     At Softylus we pay attention to the details our customers ask for that's why we will never fail to get
                      you the full picture with the frame too. We craft you the best software solutions whether 
                      it’s building up your ecommerce website, or your mobile app. As digital solution providers 
                      we do it all. Be it simple scripts or complex applications we use advanced technologies that
                       will set your business where it should be; at the top! 
                    </p>
                      <button class="button my-5 px-6 font-weight-smaill is-red is-uppercase is-rounded "><a class="has-text-white" href="<?php echo get_permalink(111); ?>"> learn more </a></button>
                </div>
                  <div class="column is-full has-text-left has-text-centered-mobile">
                 <h2 class="my-5 mt-6 line-height is-size-2-mobile is-uppercase is-size-1 is-family-softylus-black">Learn How It’s Done</h2>
                         <p class="px-3">Let’s walk you through the steps we take together.</p>
                </div>
              </div>
            </div>
          </section>
 

    <section class="section">
            <div class="container">
                <div class="columns x-secrol">
        <div class="column is-4 my-3 mx-2 about-box-bm x-item-scroll  ">
             <div class="columns is-multiline">
                   <div class="column is-full pm-0" style="padding-bottom:0 !important;">
                        <h1 class="number-styles">01</h1>
                   </div>
                   <div class="column pt-0 is-full">
                        <h2 class="is-family-softylus-black has-text-white pt-0 px-3 mb-3 font-24">Plan</h2>
                         <p class="px-3 has-text-white  mb-5">We learn about your vision in detail and study the optimal options we have for you.
</p>
                   </div>
                   
                 </div>
           </div>
       <div class="column is-4 my-3 mx-2 about-box-bm x-item-scroll  ">
             <div class="columns is-multiline">
                   <div class="column is-full pm-0" style="padding-bottom:0 !important;">
                        <h1 class="number-styles">02</h1>
                   </div>
                   <div class="column pt-0 is-full">
                        <h2 class="is-family-softylus-black has-text-white pt-0 px-3 mb-3 font-24">Design</h2>
                         <p class="px-3 has-text-white  mb-5">Leveling-up your business vision with eye-catching visuals and trendy designs
</p>
                   </div>
                   
                 </div>
           </div>
                      <div class="column is-4 my-3 mx-2 about-box-bm x-item-scroll  ">
             <div class="columns is-multiline">
                   <div class="column is-full pm-0" style="padding-bottom:0 !important;">
                        <h1 class="number-styles">03</h1>
                   </div>
                   <div class="column pt-0 is-full">
                        <h2 class="is-family-softylus-black has-text-white pt-0 px-3 mb-3 font-24">Development</h2>
                         <p class="px-3 has-text-white  mb-5">Get higher rankings with fully built websites and mobile applications</p>
                   </div>
                   
                 </div>
           </div>
                   
                </div>
            </div>
          </section>



           <section class="section ">
            <div class="container">
              <div class="columns ">
                 <div class="column  has-text-centered">
                <h1 class="is-centered is-family-softylus-black title is-2">Our Expertise</h1>
                <p class="my-5 mx-4 is-family-softylus-reg">Covering wide-range of services with top performance and execution</p>
                </div>
              </div>
            <div class="columns is-multiline has-text-centered-mobile has-text-centered-tablet ">
                <div class="column has-text-centered is-one-fifth is-half-tablet is-two-fifths-mobile"style="display:inline-block !important;">
             <figure class="is-two-fifths-mobile"style="display:inline-block !important;">
                        <img class=""  src="https://ik.imagekit.io/softylus/Group_7_Uxqx7G2z5M.svg"style="display:inline-block !important;">
                    </figure>
                </div>
                <div class="column has-text-centered is-one-fifth is-half-tablet is-two-fifths-mobile"style="display:inline-block !important;">
            <figure class="is-two-fifths-mobile" style="display:inline-block !important;">
                        <img class=""  src="https://ik.imagekit.io/softylus/Progress_Bar_-_Milestone-04_Z3MJE3XeiY.svg">
                    </figure>
                </div>
                <div class="column has-text-centered is-one-fifth is-half-tablet is-two-fifths-mobile"style="display:inline-block !important;">
              <figure class="is-two-fifths-mobile" style="display:inline-block !important;">
                        <img class=""  src="https://ik.imagekit.io/softylus/Progress_Bar_-_Milestone-02_Y0rT_aSNX.svg">
                    </figure>
                </div>
                <div class="column has-text-centered is-one-fifth is-half-tablet is-two-fifths-mobile"style="display:inline-block !important;">
               <figure class="is-two-fifths-mobile" style="display:inline-block !important;">
                        <img class=""  src="https://ik.imagekit.io/softylus/Progress_Bar_-_Milestone-03_AO2hAkrVB.svg">
                    </figure>
                </div>
                 <div class="column has-text-centered is-one-fifth is-full-tablet is-two-fifths-mobile" style="display:inline-block !important;">
               <figure class="is-two-fifths-mobile" style="display:inline-block !important;">
                        <img class=""  src="https://ik.imagekit.io/softylus/Progress_Bar_-_Milestone-01_D1-EIGEDvI7yi.svg">
                    </figure>
                </div>
            </div>
        </div>
          </section>

</div>
     

<div class="containerr pt-6 " id="portfolio" style="min-height: 100vh;    overflow: hidden; background:#1d1d1d">
    <div class="columns pb-0-mobile">
                 <div class="column .pb-0-mobile has-text-centered">
                <h1 class="is-centered is-family-softylus-black has-text-white title is-2">Our Portfolio</h1>
                <p class="my-5 mx-4 is-family-softylus-reg has-text-white">Get a fair look at our projects and decide for yourself</p>
                </div>
              </div>
    <!-- Container for all sliders, and pagination -->
    <main class="sliders-containerr mt-3">
        <!-- Here will be injected sliders for images, numbers, titles and links -->

        <!-- Simple pagination for the slider -->
        <ul class="pagination" style="margin-top:-30px !important;">
            <li class="pagination__item"><a class="pagination__button"></a></li>
            <li class="pagination__item"><a class="pagination__button"></a></li>
            <li class="pagination__item"><a class="pagination__button"></a></li>
            <li class="pagination__item"><a class="pagination__button"></a></li>
        </ul>
    </main>
 
</div>

</div></div></div>



<script
  src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
  integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs="
  crossorigin="anonymous"></script>
<script src="https://s1.softylus.com/wp-content/themes/softylus/templates/replaceimg.js" > </script>
<script src="https://s1.softylus.com/wp-content/themes/softylus/templates/momentum-slider.js"></script>
<script src="https://s1.softylus.com/wp-content/themes/softylus/templates/portfolio-carousel.js"></script>

<?php 
get_footer();
?>