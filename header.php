<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bulmapress
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="is-fullheight has-background-white" id="html">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>


    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma-carousel@4.0.3/dist/css/bulma-carousel.min.css">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">
		<?php bulmapress_skip_link_screen_reader_text(); ?>
		<header id="header" style="height: 60px">
			<!-- <nav id="site-navigation" class="navbar" role="navigation">
				<div class="navbar-brand">
					<?php bulmapress_home_link('navbar-item'); ?>
					<?php bulmapress_blog_description('navbar-item is-muted'); ?>
					<?php bulmapress_menu_toggle(); ?>
				</div>
				<div class="navbar-menu">
					<div class="navbar-start"></div>
					<?php bulmapress_navigation(); ?>
				</div>
			</nav>-->
	<nav class="navbar  is-white has-shadow is-hidden-mobile">
	<!-- logo / brand -->
	
    <div class="ml-5 pl-6 navbar-brand ">
      <a style="margin:auto;" class="navbar-item is-mobile" href="<?php echo get_site_url() ;?>">
        <img src="https://ik.imagekit.io/softylus/SOFTYLUS_small_LymDL-Ehg.png" style="max-height: 60px ; cursor: pointer;">
      </a>
	     <a   class="navbar-burger " id="burger">
        <span></span>
        <span></span>
        <span></span>
      </a>
    </div>
  
    <div class=" marginForDesktopOnly navbar-menu" id="nav-links">
      <!-- right links -->
      <div class="navbar " class=" margin: auto;">
		<?php
                    wp_nav_menu( array(
                        'theme_location' => 'primary-menu',
                        'container_class' => 'menu-menu'  ) );
                    ?>
      </div>
    </div>
  </nav>
<div style="overflow-y:hidden;height: 60px;box-shadow: rgba(0, 0, 0 ,0.1) 0px 1px 5px 1px;" class="columns is-mobile is is-multiline is-hidden-tablet m-0">
    <div class="column is-4"></div>
    <div class="column is-4 pt-0">
        <a href="<?php echo get_site_url() ;?>">
            <img src="https://ik.imagekit.io/softylus/SOFTYLUS_small_LymDL-Ehg.png" style="max-height: 55px ; cursor: pointer;" class=" my-1 ml-5 py-1">
        </a>
    </div>
</div>
    <nav id="nav" class="nav is-hidden-tablet" role="navigation">

        <!-- ACTUAL NAVIGATION MENU -->
        <?php
        wp_nav_menu( array(
            'theme_location' => 'primary-menu',
            'container_class' => 'nav_' ) );
        ?>

        <!-- MENU TOGGLE BUTTON -->
        <a href="#nav" class="nav__toggle" role="button" aria-expanded="false" aria-controls="menu">
            <svg class="menuicon" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
                <title>Toggle Menu</title>
                <g>
                    <line class="menuicon__bar" x1="13" y1="16.5" x2="37" y2="16.5"/>
                    <line class="menuicon__bar" x1="13" y1="24.5" x2="37" y2="24.5"/>
                    <line class="menuicon__bar" x1="13" y1="24.5" x2="37" y2="24.5"/>
                    <line class="menuicon__bar" x1="13" y1="32.5" x2="37" y2="32.5"/>
                    <circle class="menuicon__circle" r="23" cx="25" cy="25" />
                </g>
            </svg>
        </a>

        <!-- ANIMATED BACKGROUND ELEMENT -->
        <div class="splash"></div>

    </nav>

		</header>
		<div id="content" class="site-content has-background-white">


