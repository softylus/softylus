
const nav = document.querySelector('#nav');
const nav_ = document.querySelector('.nav_');
const footercontent = document.querySelector('.footer');
const site_content = document.querySelector('.site-content');
const menuToggle = document.querySelector('.nav__toggle');
const portfolio = document.querySelector('#portfolio');
let isMenuOpen = false;


// TOGGLE MENU ACTIVE STATE
menuToggle.addEventListener('click', e => {
    e.preventDefault();
    isMenuOpen = !isMenuOpen;

    // toggle a11y attributes and active class
    menuToggle.setAttribute('aria-expanded', String(isMenuOpen));
    nav_.hidden = !isMenuOpen;
    nav.classList.toggle('nav--open');
    site_content.classList.toggle('hide');
    footercontent.classList.toggle('hide');
    portfolio.classList.toggle('hide');
});



// TRAP TAB INSIDE NAV WHEN OPEN
// nav.addEventListener('keydown', e => {
//     // abort if menu isn't open or modifier keys are pressed
//     if (!isMenuOpen || e.ctrlKey || e.metaKey || e.altKey) {
//         return;
//     }
//
//     // listen for tab press and move focus
//     // if we're on either end of the navigation
//     const menuLinks = menu.querySelectorAll(a);
//     if (e.keyCode === 9) {
//         if (e.shiftKey) {
//             if (document.activeElement === menuLinks[0]) {
//                 menuToggle.focus();
//                 e.preventDefault();
//             }
//         } else if (document.activeElement === menuToggle) {
//             menuLinks[0].focus();
//             e.preventDefault();
//         }
//     }
// });
const menu = document.querySelector(".menu");
const items = document.querySelectorAll(".item");

/* Activate Submenu */
function toggleItem() {
    if (this.classList.contains("sub-menu-active")) {
        this.classList.remove("sub-menu-active");
    } else if (menu.querySelector(".sub-menu-active")) {
        menu.querySelector(".sub-menu-active").classList.remove("sub-menu-active");
        this.classList.add("sub-menu-active");
    } else {
        this.classList.add("sub-menu-active");
    }
}

/* Close Submenu From Anywhere */
function closeSubmenu(e) {
    let isClickInside = menu.contains(e.target);

    if (!isClickInside && menu.querySelector(".sub-menu-active")) {
        menu.querySelector(".sub-menu-active").classList.remove("sub-menu-active");
    }
}
/* Event Listeners */
for (let item of items) {
    if (item.querySelector(".sub-menu")) {
        item.addEventListener("click", toggleItem, false);
    }
    item.addEventListener("keypress", toggleItem, false);
}
document.addEventListener("click", closeSubmenu, false);